package ru.tsc.anaumova.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.model.AbstractUserOwnedModel;

@Repository
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

}