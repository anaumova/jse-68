package ru.tsc.anaumova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.service.ProjectService;

import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    private List<Project> getProjects() {
        return projectService.findAll();
    }

    @GetMapping("/project/create")
    public String create() {
        projectService.add("new project " + System.currentTimeMillis());
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectService.removeById(id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@ModelAttribute("project") Project project, BindingResult result) {
        projectService.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Project project = projectService.findById(id);
        return new ModelAndView("project-edit", "project", project);
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/projects")
    public ModelAndView list() {
        return new ModelAndView("project-list", "projects", getProjects());
    }

}