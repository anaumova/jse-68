package ru.tsc.anaumova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

}