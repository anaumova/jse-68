package ru.tsc.anaumova.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    void init(@NotNull MessageListener listener);

}